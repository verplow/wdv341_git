<!DOCTYPE html>
<html>
  <head>
    <title> CariBeneshHomeworkPage</title>
	<style type = "text/css">
		body {
			 font-family: Arial, Helvetica, sans-serif;
			 background-color: aliceblue;
			 margin: 5px 20px;
		}
		h1 {
			background-color: grey;
			color: white;
			font-weight: bold;
			text-align: center;
		}
		h2  {
			color: navy;
			font-weight: bold;
			text-align: center;
		}
		h3  {
			background-color: teal;
			color: white;
			text-align: center;
		}
		h4  {
			text-align: center;
		}
	</style>
  </head>
  <body>
		<h1>Cari Benesh</h1>
		<p><h2>WDV341 Homework Page</h2></p>
		<p><h3> Assignments </h3></p>
		<p><h4><a href = "testGit.php">TestGit Assignment</a></h4></p>
		<p><h4><a href = "phpBasics.php">PHP Basics Assignment</a></h4></p>
		<p><h4><a href = "phpFunctions.php">PHP Functions Assignment</a></h4></p>
		<p><h4><a href = "processEmail.php">PHP Email Class Assignment</a></h4></p>
		<p><h4><a href = "emailForm.html">PHP Contact Form with Email Assignment</a></h4></p>
		<p><h4><a href = "WDV341 PHP Form Handler/exampleForm.html">PHP Form Handler Assignment</a></h4></p>
		<p><h4><a href = "formValidationAssignment.php">Self-Posting Form</a></h4></p>
    <p><h4><a href = "studentInfoForm/studentInfoForm2.php">Student Info Form</a></h4></p>
    <p><h4><a href = "connectPDO.php">Connection file</a></h4></p>
    <p><h4><a href = "eventsForm2.php">Events Form</a></h4></p>
    <p><h4><a href = "selectEvents.php">Select Event Form</a></h4></p>
    <p><h4><a href = "selectOneEvent.php">Select One Event</a></h4></p>
    <p><h4><a href = "modifySelectEvents.php">Modify Select Events</a></h4></p>
    <p><h4><a href = "deleteEvent.php">Delete Event</a></h4></p>
  </body>
</html>
