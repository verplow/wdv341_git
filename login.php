<?php
session_start();

$event_user_name = "";
$event_user_password = "";
$errorMsg = "";
$message = "";
$_SESSION['validUser'] = "";
//if valid user already logged in, display admin options
if($_SESSION['validUser'] == "true")
{
    // admin functions here
  $message = "Welcome " .$event_user_name;
}
else
{
  // if form has been submitted, validate input
  if(isset($_POST["submit"]))
  {
    $event_user_name = $_POST["username"];
    $event_user_password = $_POST["password"];

    // not working
    function testInput($inData)
    {
      $inData = trim($inData);
      $inData = stripslashes($inData);
      $inData = htmlspecialchars($inData);
      return $inData;
    }

    //echo $event_user_name;
    //validate input if not blank
    //if(strlen($event_user_name) > 0 && strlen($event_user_password) > 0)
    //{
      //SQL query to check login against $database
      try
      {
        //include 'connectPDO.php';
        include 'connect.php';

        $sql= "SELECT event_user_name, event_user_password FROM event_user WHERE event_user_name = :event_user_name AND event_user_password = :event_user_password";
        $query = $conn->prepare($sql);
        $query->bindParam(":event_user_name", $event_user_name);
        $query->bindParam(":event_user_password", $event_user_password);
        $query->execute();

        $query->setFetchMode(PDO::FETCH_ASSOC);

  		  $row=$query->fetch(PDO::FETCH_ASSOC);

        //get
  			$validName=$row['event_user_name'];
  			$validPass=$row['event_user_password'];
        //get result count
        //$stmt = "SELECT count() FROM event_user "

        // if entered name and password match table username and password
        if ($event_user_name == $validName && $event_user_password == $validPass)
        {
          $_SESSION['validUser'] = "true";
          $message = "Welcome back, " .$event_user_name;
          //display admin options
        }
        else
        {
          $_SESSION['validUser'] = "false";
          $message = "Invalid login - please re-enter";
        }
      } // end try
      catch (PDOException $e)
      {
        echo $e->getMessage();
      }
    //}
    //else empty, set error message
    /*else
    {
      $errorMsg = "Both fields are required.  Please resubmit";
    }//end validation */
  }
  else
  {
  // has not seen form, display form
  }

  ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name-"viewport" content="width=device-width, initial-scale=1"/>
    <title>Administration Page</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  </head>
  <body>
      <h1>Login Page</h1>
      <?php
      if($_SESSION['validUser'] == "true")
      {
        //echo $errorMsg;
        echo $message; ?>
          <nav>
            <ul>
                <li><a href="displayEvents.php">Today's Events</a></li>
                <li><a href="selectEvents.php">Edit Events</a></li>
                <li><a href="eventsForm2.php">Add Event</a></li>
                <li><a href="logout.php">Sign Out</a></li>
            </ul>
          <div class="clearFloat"></div>

        </nav>
        <?php
      }
      else
      {
          echo $message; ?>
        <form id="login" name="login" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
         <div>
           <label for="username"><b>User Name: </b></label>
           <input type="text" placeholder="Enter Username" name="username" value="">

           <label for"psw"><b>Password</b></label>
           <input type="password" placeholder="Enter Password" name="password" value="">
           <span><?php echo $errorMsg; ?></span>

           <input type="submit" name="submit" id="submit" value="Login">
           <label>
             <input type="checkbox" checked="checked" name="remember"> Remember me
           </label>
         </div>

         <div style="background-color: $flflfl">
           <button type="button">Cancel</button>
           <span>Forgot <a href="#">password?</a></span>
         </div>
       </form>
     <?php
    }
  } // end else statement
  ?>
  </body>
</html>
