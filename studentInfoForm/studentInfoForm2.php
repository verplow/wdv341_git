<?php
//fields
$fname = "";
$lname = "";
$program1 = "";
$program2 = "";
$webAdd1 = "";
$webAdd2 = "";
$linkedIn = "";
$email = "";
$hometown = "";
$careerGoals  = "";
$threeWords = "";
$fnameErrMsg = "";
$lnameErrMsg = "";
$programErrMsg = "";
$urlErrMsg= "";
$linkedInErrMsg = "";
$emailErrMsg = "";
$hometownErrMsg = "";
$goalsErrMsg = "";
$threeWordsErrMsg = "";
// global validForm variable
$validForm = false;

// check for form submission
if(isset($_POST["submitBio"]))
{
  $message = "Posted";
  // get name-value pairs
  $fname = $_POST["firstName"];
  $lname = $_POST["lastName"];
  $program1 = $_POST["program"];
  $program2 = $_POST["program2"];
  $webAdd1 = $_POST["websiteAddress"];
  $webAdd2 = $_POST["websiteAddress2"];
  $linkedIn = $_POST["linkedIn"];
  $email = $_POST["email"];
  $hometown = $_POST["hometown"];
  $careerGoals = $_POST["careerGoals"];
  $threeWords = $_POST["threeWords"];

  //validateFirstName
  // valid first name should only include letters, numbers, and spaces
  // ... must be present
  function validateFirstName($inName)
  {
    global $fnameErrMsg, $validForm;
    // checks for name with space or at least one character/number entered
    if(preg_match('/^[\w\s]+$/', $inName))
    {
      if (strlen($inName != 0))
      {
        $validForm = true;
        $fnameErrMsg = "";
      }
    }
    else
    {
      $validForm = false;
      $fnameErrMsg = "Invalid first name, please re-enter " . $inName;
    }
  }

    //validateLastName
    // valid last name should only include letters, numbers and spaces
    // ... must be present
    function validateLastName($inName)
    {
      global $lnameErrMsg, $validForm;
      // checks for name with space or at least one character/number entered
      if(preg_match('/^[\w\s]+$/', $inName))
      {
        if (strlen($inName != 0))
        {}
      }
      else
      {
        $validForm = false;
        $lnameErrMsg = "Invalid last name, please re-enter ";
      }
    }

    //validateProgram
  	//valid program must not be default options
    function validateProgram($inProgram)
    {
      global $programErrMsg, $validForm;
      // checks for default selection
      if($inProgram == "default")
      {
        $validForm = false;
        $programErrMsg = "Invalid program selection, please re-select ";
      }
    }

    //validateWebsiteAddress
    function validateWebsiteAddress($inURL)
    {
      global $urlErrMsg, $validForm;
      // remove illegal characters
      $inURL = filter_var($url, FILTER_SANITIZE_URL);

      //valid URL format
      if(filter_var($inURL, FILTER_VALIDATE_URL))
      {}
      else
      {
        $validForm = false;
        $urlErrMsg = "Invalid web address, please re-enter ";
      }
    }

    function validateLinkedIn($inLinkedIn)
    {
      global $linkedInErrMsg, $validForm;
      // remove illegal characters
      $inURL = filter_var($inLinkedIn, FILTER_SANITIZE_URL);

      // valid linkedIn address
      if(!preg_match('/http(s)?:\/\/([w]{3}\.)?linkedin\.com\/in\/([a-zA-Z0-9-]{5,30})\/?/', $inLinkedIn))
      {
        $validForm = false;
        $linkedInErrMsg = "Invalid LinkedIn address, please re-enter ";
      }
    }
    //validateHometown
  	// valid name should only include letters, numbers, spaces, and commas
  	// ... must be present
    function validateHometown($inTown)
    {
      global $townErrMsg, $validForm;
      if(preg_match('/^[\w\s\,\.]+$/', $inTown))
      {
        if(strlen($inTown != 0))
        {}
      }
      else
      {
        $validForm = false;
        $townErrMsg = "Invalid town, please re-enter ";
      }
    }

    //valid email address
    function validateEmail($inEmail)
    {
      global $emailErrMsg, $validForm;
      //remove illegal characters
      $inEmail = filter_var($inEmail, FILTER_SANITIZE_EMAIL);
      //validate email format
      if(filter_var($inEmail, FILTER_VALIDATE_EMAIL))
      {}
      else
      {
        $validForm = false;
        $emailErrMsg = "Invalid email address, please re-enter ";
      }
    }

  	//valid career goals should include only numbers, letters, spaces, and basic punctuation
    function validateCareerGoals($inGoals)
    {
      global $goalsErrMsg, $validForm;
      if(preg_match('/^[\w\s\,\.\?\!\:\;\"\']+$/', $inGoals))
      {}
      else
      {
        $validForm = false;
        $goalsErrMsg = "Invalid entry, please re-enter ";
      }
    }

    //validateThreeWords
  	//valid three-words should include only numbers, letters, spaces, and basic punctuation
    function validateThreeWords($inWords)
    {
      global $threeWordsErrMsg, $validForm;
      if(preg_match('/^[\w\s\,\.\?\!\:\;\"\']+$/', $inWords))
      {}
      else
      {
        $validForm = false;
        $threeWordsErrMsg = "Invalid entry, please re-enter ";
      }
    }

    // validate fieldset$validForm = true;		//switch for keeping track of any form validation errors
    $validForm= true;
		validateFirstName($fname);
		validateLastName($lname);
		validateEmail($email);
    validateLinkedIn($linkedIn);
    validateHometown($hometown);
    validateProgram($program1);
    validateCareerGoals($careerGoals);
    validateThreeWords($threeWords);

		if($validForm)
		{
			$message = "Success!";
		}
		else
		{
			$message = "Please correct the errors below:";
		}
}

else {
  // page not viewed yet
}
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>DMACC Portfolio Day Bio Form</title>
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="css/normalize.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">

  <!-- css3-mediaqueries.js for IE less than 9 -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="css3-mediaqueries.js"></script>
<script src="jquery-3.2.1.js"></script>
<script>
	$(document).ready(function(){
		if( $("select[name=program]	option:selected").val() == "webDevelopment")
		{
			$(".secondWeb").css("display", "inline");
		}
		else
		{
			$(".secondWeb").css("display", "none");
		}

		$("select#program").change(function(){
			if( $("select#program option:checked").val() == "webDevelopment")
			{
				$(".secondWeb").css("display", "inline");
			}
			else
			{
				$(".secondWeb").css("display", "none");
			}
		});

		function resetForm(){
			$("#firstName").val("");
			$("#lastName").val("");
			$("#program").val("default");
			$("#websiteAddress").val("");
			$("#websiteAddress2").val("");
			$("#email").val("");
			$("#hometown").val("");
			$("#careerGoals").val("");
			$("#threeWords").val("");
		}
	});
</script>

  <style>
	img{
		display: block;
		margin: 0 auto;
	}
	.frame{
		background-image: url("orange popsicle.jpg");
		padding: 1em;
	}
	.frame2{
		background-image: url("citrus.jpg");
		padding: 1.3em;
	}
	body{
		background-image: url("bodacious.png");
		margin: 1.5em;
	}

	.main {
		padding: 1em;
		background-color: white;
	}
	form{
		text-align: center;
	}
	h2 {
		text-align: center;
	}
	.robotic{
		display: none;
	}

	.form {
		background-color:white;
		padding-left: 5em;
	}
	p {
		align:left;
	}
	.citrus{
		margin: auto;
		background-image: url("raspberry.jpg");
		padding: 1.3em;
		width: 70%;
	}
	.bamboo{
		background-image: url("bamboo.jpg");
		padding: 1em;
	}
	.violet{
		background-image: url("ultra violet.png");
		padding: .5em;
	}
	.secondWeb{
		display: none;
	}
	table{
		margin: auto;
	}
	table td{
		padding-bottom: .75em;
	}
	.error{
		font-style: italic;
		color: #d42a58;
		font-weight: bold;
	}

@media only screen and (max-width:620px) {
  /* For mobile phones: */
  img {
    width:100%;
  }
  .form {
	width:100%;
	padding-left: .1em;
	padding-top: .1em;
  }
  .citrus {
	background-image:none;
  }
  .bamboo {
	background-image:none;
  }
  .violet {
	background-image:none;
  }
  /*.secondWeb{
		display: none;
	}*/
  table{
		margin: auto;
	}
  table td{
		padding-bottom: .5em;
	}
}
</style>

</head>

<section class="orange">
<body>
<div class="frame2">
<div class="frame">

  <div class="main">
  <div><img src="madonna.gif" alt="Mix gif" ></div>
  <br>

<!--<a href = 'dmaccPortfolioDayLogout.php'>Log Out</a>-->

<section class="citrus">
<section class="bamboo">
<section class="violet">

	<div class="main form">

	<h2></h2>
	</table>
  <div>
    <?php
          //If the form was submitted and valid and properly put into database display the INSERT result message
    if($validForm)
    {
      ?>
          <h2><?php echo "$message"; ?></h2>

      <?php
    }
    else	//display form
    {
      ?> <h2><?php echo "$message"; ?></h2>
  </div>
    <form id="portfolioBioForm" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">

      <table>
      <tr>
      <td>Login Email:<br> <input type="text" id="emailToLogin" name="emailToLogin" value=""/></td>
      </tr>
      <tr>
      <td>First Name:<br> <input type="text" id="firstName" name="firstName" value="<?php echo $fname; ?>"/><br>
        <span class="error" id="$fnameErrMsg"><?php echo $fnameErrMsg; ?></span></td>
      </tr>
      <tr>
      <td>Last Name:<br> <input type="text" id="lastName" name="lastName" value="<?php echo $lname; ?>" /><br>
        <span class="error" id="lastNameError"><?php echo $lnameErrMsg; ?></span></td>
      </tr>
      <tr>
      <td >Program:<br> <select id="program" name="program">
          <option value="default" <?php if ($_POST['program']=='default') echo 'selected="selected";'?>)>---Select Your Program---</option>
          <option value="animation" <?php if ($_POST['program']=='animation') echo 'selected="selected";'?>)>Animation</option>
          <option value="graphicDesign" <?php if ($_POST['program']=='graphicDesign') echo 'selected="selected";'?>)>Graphic Design</option>
          <option value="photography" <?php if ($_POST['program']=='photography') echo 'selected="selected";'?>)>Photography</option>
          <option value="videoProduction" <?php if ($_POST['program']=='videoProduction') echo 'selected="selected";'?>)>Video Production</option>
          <option value="webDevelopment" <?php if ($_POST['program']=='webDevelopment') echo 'selected="selected";'?>)>Web Development</option>
        </select><br><span class="error" id="programError"><?php echo $programErrMsg; ?></span></td>
      </tr>
      <tr>
      <td >Secondary Program:<br> <select id="program2" name="program2">
          <option value="none" <?php if ($_POST['program']=='none') echo 'selected="selected";'?>)>---No Secondary Program---</option>
          <option value="animation" <?php if ($_POST['program']=='animation') echo 'selected="selected";'?>)>Animation</option>
          <option value="graphicDesign" <?php if ($_POST['program']=='graphicDesign') echo 'selected="selected";'?>)>Graphic Design</option>
          <option value="photography" <?php if ($_POST['program']=='photography') echo 'selected="selected";'?>)>Photography</option>
          <option value="videoProduction" <?php if ($_POST['program']=='videoProduction') echo 'selected="selected";'?>)>Video Production</option>
          <option value="webDevelopment" <?php if ($_POST['program']=='webDevelopment') echo 'selected="selected";'?>)>Web Development</option>
        </select><br><span class="error" id="program2Error"></span></td>
      </tr>
      <tr>
      <td>Website Address:<br> <input type="text" id="websiteAddress" name="websiteAddress" value="<?php echo $webAdd1; ?>"/><br>
        <span class="error" id="websiteAddressError"><?php echo $urlErrMsg; ?></span></td>
      </tr>
      <tr>
      <td>Personal Email:<br><input type="text" id="email" name="email" value="<?php echo $email; ?>" /><br>
        <span class="error" id="emailError"><?php echo $emailErrMsg; ?></span></td>
      </tr>
      <tr>
      <td>LinkedIn Profile:<br><input type="text" id="linkedIn" name="linkedIn" value="<?php echo $linkedIn; ?>" /><br>
        <span class="error" id="linkedInError"><?php echo $linkedInErrMsg; ?></span></td>
      <tr>
      <td><span id="secondWeb" style="display: none">Secondary Website Address (git repository, etc.):<br> <input type="text" id="websiteAddress2" name="websiteAddress2" value=""/><br>
        <span class="error" id="websiteAddress2Error"><?php echo $urlErrMsg; ?></span></span></td>
      </tr>
      <tr>
      <td>Hometown:<br> <input type="text" id="hometown" name="hometown" value="<?php echo $hometown; ?>"/><br>
        <span class="error" id="hometownError"><?php echo $townErrMsg; ?></span></td>
      </tr>
      <tr>
      <td>Career Goals:<br> <textarea id="careerGoals" name="careerGoals" value="<?php echo $careerGoals; ?>"> </textarea><br>
        <span class="error" id="careerGoalsError"><?php echo $goalsErrMsg; ?></span></td>
      </tr>
      <tr>
      <td>3 Words that Describe You:<br> <input type="text" id="threeWords" name="threeWords" value="<?php echo $threeWords; ?>"/><br>
        <span class="error" id="threeWordsError"><?php echo $threeWordsErrMsg; ?></span></td>
      <p class="robotic" id="pot">
        <label>Leave Blank</label>
        <input type="hidden" name="inRobotest" id="inRobotest" class="inRobotest" />
      </p>
      <input type="hidden" id="submitConfirm" name="submitConfirm" value="submitConfirm"/>
      </tr>
      <tr>
      <td><input type="submit" id="submitBio" name="submitBio" value="Submit Bio" /></td>
      </tr>
      <tr>
      <td><input type="reset" id="resetBio" name="resetBio" value="Reset Bio"/></td>
      </tr>
      </table>
    </form>
  <?php } ?>
    </div>


  </section>
  </section>
  </section>

  </div>

  </body>
  </section>

  </html>
