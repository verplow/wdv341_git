<?php

function dateUS($inDate)
{
	$date=date_create($inDate);
	echo date_format($date, "m/d/Y");
}

function dateWorld($inDate)
{
	$date=date_create($inDate);
	echo date_format($date, "d/m/Y");
}

function testString($inString)
{
	$charCount = strlen($inString);		// gets number of characters
	$trimString = trim($inString, ""); 	// trims leading or ending whitespace
	$lowString = strtolower($trimString); // changes trimmed string input to lowercase
	$searchTerm = "DMACC";				// set search term
	$lowSearch = strtolower($searchTerm); // use lower case to search converted lower case input
	
	echo "Character count: " .$charCount. "<br>";		// display char count
	echo "Lower Case string: " .$lowString. "<br>";		// display lower case string
	
	// check for 'DMACC' string in either lower or upper case
	if(strpos($lowString, $lowSearch) !== false)
	{
		echo "The string " . $searchTerm . " was found. <br>";
	}
	else
	{
		echo "The string '" . $searchTerm . "' was not found. <br>";
	}	
}

function formatNum()
{
	echo number_format("1234567890",2);		// sets decimal place to 2
}

function formatCurr()
{
	setlocale(LC_MONETARY, 'en_US');		// sets local currency type
	echo money_format('%n', "123456");		// %n sets local currency formatting
}

?>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP Function Examples</title>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>PHP Functions - Example Code</h2>

<h3>Date functions Example</h3>
<p>1. <?php echo dateUS("Jan 31 2018"); ?></p>
<p>2. <?php echo dateWorld("Dec 2 2018"); ?></p>

<h3>String functions Example</h3>
<p> <?php echo testString(" All for one and one for all "); ?></p>

<h3> Number function Example</h3>
<p> <?php echo formatNum(); ?></p>

<h3> Currency function Example</h3>
<p> <?php echo formatCurr(); ?></p>
<p>&nbsp;</p>
</body>
</html>