
<?php
session_start();

if ($_SESSION['validUser'] == "true")
{
  $message = "in valid user";
  //include "connectPDO.php";
  include "connect.php";
  $validForm = "false";

  if(isset($_POST["submitForm"]))
  {
    //The form has been submitted and needs to be processed

    //Validate the form data here!

    //Get the name value pairs from the $_POST variable into PHP variables
    //This example uses PHP variables with the same name as the name atribute from the HTML form
    $item_code = $_POST["item_code"];
    $item_name = $_POST["item_name"];
    $vendor_name = $_POST["vendor_name"];
    $upc = $_POST["upc"];
    $item_image = $_POST["item_image"];
    $item_cost = $_POST["item_cost"];
    $item_price = $_POST["item_price"];
    $on_hand = $_POST["on_hand"];

    //VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.
      function validateString($inName)
      {
        global $validForm, $nameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
        $stringErrMsg = "";

        if($inName == "")
        {
          $validForm = false;
          $stringErrMsg = "Field cannot be blank";
        }
      }//end validateString()

      function validateInteger($inInt)
      {
        global $validForm, $intErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
        $intErrMsg = "";
        if(preg_match ("/[^0-9]/", $inInt) || $inInt != "")
        {}
        else
        {
          $validForm = false;
          $intErrMsg = "Invalid entry";
        }
      }//end validateInteger()

      function validateCurrency($inCurrency)
      {
        global $validForm, $currencyErrMsg;
        $currencyErrMsg = "";

        if(is_numeric($inCurrency) || $inCurrency != "")
        {}
        else
        {
          $validForm = false;
          $currencyErrMsg = "Invalid entry";
        }
      }// end validateCurrency()

      //VALIDATE FORM DATA  using functions defined above
      $validForm = true;		//switch for keeping track of any form validation errors

      validateString($item_name);
      validateString($vendor_name);
      validateInteger($upc);
      validateInteger($item_code);
      validateInteger($on_hand);
      validateCurrency($item_cost);
      validateCurrency($item_price);

      if($validForm)
      {
        $message = "All good";
        //}
        try
        {
          //nclude "connectPDO.php";
          include "connect.php";
          echo "Connected successfully";

          $sql = "INSERT INTO products (";
          $sql .= "item_code, ";
          $sql .= "item_name, ";
          $sql .= "vendor_name, ";
          $sql .= "upc, ";
          $sql .= "item_image, ";
          $sql .= "item_cost, ";
          $sql .= "item_price, ";
          $sql .= "on_hand";
          $sql .= ") VALUES (:item_code, :item_name, :vendor_name, :upc, :item_image, :item_cost, :item_price, :on_hand)";

          //Display the SQL command to see if it correctly formatted.*/
          //echo "<p>$sql</p>";

          $stmt = $conn->prepare($sql);	//Prepares the query statement
          //Binds the parameters to the query.
          //The ssssis are the data types of the variables in order.
          $stmt->bindParam(':item_code', $item_code);
          $stmt->bindParam(':item_name', $item_name);
          $stmt->bindParam(':vendor_name', $vendor_name);
          $stmt->bindParam(':upc', $upc);
          $stmt->bindParam(':item_image', $item_image);
          $stmt->bindParam(':item_cost', $item_cost);
          $stmt->bindParam(':item_price', $item_price);
          $stmt->bindParam(':on_hand', $on_hand);
          $stmt->execute();

          $result = $stmt;  // hold $query value for if-check so query doesn't execute twice
          //echo "<p>"$result"</p>";
          //Run the SQL prepared statements
          if ( $result )
          {
          	$message = "<h3>Your record has been successfully added to the database.</h3>";
          	$message .= "<p>Please <a href='displayProducts.php'>view</a> your records.</p>";
          }
          else
          {
          	$message = "<h1>You have encountered a problem.</h1>";
          	//$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";	//remove this for production purposes
          }
        } // end try
        catch(PDOException $e)
        {
          ?> <hr><hr>
          <?php
          echo "Connection failed: " . $e->getMessage();
        }
      $conn->close;
      //$connection->close();	//closes the connection to the database once this page is complete.
    }// end ifvalid
    else
    {
        $message = "Submission error";
    } // end not valid
  }// ends ifIsSet
  else
  {
    //Form has not been seen by the user.  display the form
    $message = "Please fill out form";
  }
/*}//end Valid User True
else
{
  //Invalid User attempting to access this page. Send person to Login Page
  	header('Location: login.php');
}*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!--end login links-->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name-"viewport" content="width=device-width, initial-scale=1"/>
  <title>Add New Item</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="displayProducts.php">Display Products</a></li>
        <li><a href="editProducts.php">Edit Products</a></li>
        <li><a href="addProducts.php">Add Products</a></li>
        <li><a href="emailForm.php">Contact Us</a></li>
        <li><a href="logout.php">Sign Out</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
<body>
  <hr>
  <?php
  if(isset($_POST["submitForm"]))
  {
	   //Display the following line when the form has been submitted and
	   //the SQL query has successfully updated the database.
     //header('location:addProducts.php');
    if(validForm)
    { ?>
       <hr>
       <div class="container">
          <h1 class="welcome text-center"></h1>
         <div class="card card-container">
            <h2 class="login_title text-center">Add Item</h2>
              <hr>
              <h5 class="text-center"><?php echo $message; ?><br</h5>
          </div>
        </div>
      </div> <!--close container--><?php
    }
    else // not valid
    {
      //Display the following lines if the page is called from a link.
      //The user has not seen the form yet and needs to see the form.
      //This will display the form, allow the user to enter data, then submit the form*/
      ?>
        <div class = "container">
          <h1 class="welcome text-center"></h1>
             <div class="card card-container">
                <h2 class="login_title text-center">Add Items</h2>
                  <hr>
          	        <h3 class="login_title text-center"><?php echo $message; ?></h3>
              <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
                <div class="row">
                  <div class="col-sm-5">
                    Item Code:
                  </div>
                  <div class="col-sm-5">
                    <input type="text" name="item_code" id="item_code" value = "<?php echo $item_code; ?>"/>
                    <span><?php echo $intErrMsg; ?></span>
                  </div>
                </div>
              </br>
                <div class="row">
                  <div class="col-sm-5">
                    Item Name:
                  </div>
                  <div class="col-sm-5">
                    <input type="text" name="item_name" id="item_name" value = "<?php echo $item_name; ?>"/>
                    <span><?php echo $stringErrMsg; ?></span>
                  </div>
                </div>
              </br>
              <div class="row">
                <div class="col-sm-5">
                  Vendor Name:
                </div>
                <div class="col-sm-5">
                  <input type="text" name="vendor_name" id="vendor_name" value = "<?php echo $vendor_name; ?>"/>
                  <span><?php echo $stringErrMsg; ?></span>
                </div>
              </div>
            </br>
              <div class="row">
                <div class="col-sm-5">
                  UPC Code:
                </div>
                <div class="col-sm-5">
                  <input type="text" name="upc" id="upc" value = "<?php echo $upc; ?>"/>
                  <span><?php echo $intErrMsg; ?></span>
                </div>
              </div>
            </br>
              <div class="row">
                <div class="col-sm-5">
                  Image name:
              </div>
              <div class="col-sm-5">
                <input type="text" name="item_image" id="item_image" value = "<?php echo $item_image; ?>"/>
                <span><?php //echo $nameErrMsg; ?></span>
              </div>
            </div>
            </br>
              <div class="row">
                <div class="col-sm-5">
                  Item Cost:
                </div>
                <div class="col-sm-5">
                  <input type="text" name="item_cost" id="item_cost" value = "<?php echo $item_cost; ?>"/>
                  <span><?php echo $currencyErrMsg; ?></span>
                </div>
              </div>
            </br>
              <div class="row">
                <div class="col-sm-5">
                  Item Price:
                </div>
                <div class="col-sm-5">
                    <input type="text" name="item_price" id="item_price" value="<?php echo $item_price; ?>"/>
                    <span><?php echo $currencyErrMsg; ?></span>
                </div>
              </div>
            </br>
              <div class="row">
                <div class="col-sm-5">
                  On Hand:
                </div>
                <div class="col-sm-5">
                    <input type="text" name="on_hand" id="on_hand" value="<?php echo $on_hand; ?>"/>
                    <span><?php echo $intErrMsg; ?></span>
                </div>
              </div>
            </br>
                <p class = "text-center">
                  <input type="submit" name="submitForm" id="submitForm" value="Submit" />
                  <input type="reset" name="button2" id="button2" value="Reset" />
                </p>
              </form>
              <p>&nbsp;</p>
            </div>
        </div>
        <?php
      }
      ?>
    </body>
    </html><?
  }// end if Isset
  else
  {
    //Display the following lines if the page is called from a link.
    //The user has not seen the form yet and needs to see the form.
    //This will display the form, allow the user to enter data, then submit the form*/
    ?>
      <div class = "container">
        <h1 class="welcome text-center"></h1>
           <div class="card card-container">
              <h2 class="login_title text-center">Add Items</h2>
                <hr>
                  <h3 class="login_title text-center"><?php echo $message; ?></h3>
            <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
              <div class="row">
                <div class="col-sm-5">
                  Item Code:
                </div>
                <div class="col-sm-5">
                  <input type="text" name="item_code" id="item_code" value = "<?php echo $item_code; ?>"/>
                  <span><?php echo $intErrMsg; ?></span>
                </div>
              </div>
            </br>
              <div class="row">
                <div class="col-sm-5">
                  Item Name:
                </div>
                <div class="col-sm-5">
                  <input type="text" name="item_name" id="item_name" value = "<?php echo $item_name; ?>"/>
                  <span><?php echo $stringErrMsg; ?></span>
                </div>
              </div>
            </br>
            <div class="row">
              <div class="col-sm-5">
                Vendor Name:
              </div>
              <div class="col-sm-5">
                <input type="text" name="vendor_name" id="vendor_name" value = "<?php echo $vendor_name; ?>"/>
                <span><?php echo $stringErrMsg; ?></span>
              </div>
            </div>
          </br>
            <div class="row">
              <div class="col-sm-5">
                UPC Code:
              </div>
              <div class="col-sm-5">
                <input type="text" name="upc" id="upc" value = "<?php echo $upc; ?>"/>
                <span><?php echo $intErrMsg; ?></span>
              </div>
            </div>
          </br>
            <div class="row">
              <div class="col-sm-5">
                Image name:
            </div>
            <div class="col-sm-5">
              <input type="text" name="item_image" id="item_image" value = "<?php echo $item_image; ?>"/>
              <span><?php //echo $nameErrMsg; ?></span>
            </div>
          </div>
          </br>
            <div class="row">
              <div class="col-sm-5">
                Item Cost:
              </div>
              <div class="col-sm-5">
                <input type="text" name="item_cost" id="item_cost" value = "<?php echo $item_cost; ?>"/>
                <span><?php echo $currencyErrMsg; ?></span>
              </div>
            </div>
          </br>
            <div class="row">
              <div class="col-sm-5">
                Item Price:
              </div>
              <div class="col-sm-5">
                  <input type="text" name="item_price" id="item_price" value="<?php echo $item_price; ?>"/>
                  <span><?php echo $currencyErrMsg; ?></span>
              </div>
            </div>
          </br>
            <div class="row">
              <div class="col-sm-5">
                On Hand:
              </div>
              <div class="col-sm-5">
                  <input type="text" name="on_hand" id="on_hand" value="<?php echo $on_hand; ?>"/>
                  <span><?php echo $intErrMsg; ?></span>
              </div>
            </div>
          </br>
              <p class = "text-center">
                <input type="submit" name="submitForm" id="submitForm" value="Submit" />
                <input type="reset" name="button2" id="button2" value="Reset" />
              </p>
            </form>
            <p>&nbsp;</p>
          </div>
      </div>
      <?php
    }
    ?>
  </body>
  </html><?
}//end Valid User True
else
{
  //Invalid User attempting to access this page. Send person to Login Page
  	header('Location: login.php');
}
?>
