<?php
session_start();
if ($_SESSION['validUser'] == "true")
{
  //redirect to selectEvents.php
  header('location:editProducts.php');
  //include 'connectPDO.php';
  include "connect.php";
  // get 'itemID to delete'
  $id = $_GET['item_id'];
  $stmt = $conn->prepare("DELETE FROM products WHERE item_id = $id");
  $stmt -> execute();
  exit();
}
else {
  // invalid username, send to login page
  header("Location: login.php");
}
?>

<h1>Administration Page</h1>
<h2><?php echo "Welcome " .$username ?></h2>
<nav>
  <ul>
    <li><a href="displayProducts.php">Display Products</a></li>
    <li><a href="editProducts.php">Edit Products</a></li>
    <li><a href="addProducts.php">Add Products</a></li>
    <li><a href="emailForm.php">Contact Us</a></li>
    <li><a href="logout.php">Sign Out</a></li>
  </ul>
</nav>
<?php
