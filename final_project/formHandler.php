
<?php
	session_start();

	include("email.php");

	$name = $_POST["contactName"];
	$email = $_POST["contactEmail"];
	$comments = $_POST["comments"];
	$sender = "cbenesh.com@domainsbyproxy.com";
	$subject = "Confirmation";

	// set up email message
	$email_body = "Your form has been received. We will respond within 48 hours.\n".  "Here are the comments:\n " .$comments;

	// send email
	$newEmail = new Email();
	$newEmail->setSender($sender);
	$newEmail->setSendTo($email);
	$newEmail->setSubject($subject);
	$newEmail->setMessage($email_body);
	$newEmail->sendEmail();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name-"viewport" content="width=device-width, initial-scale=1"/>
<title>Events Form</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!--end login links-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</head>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<!--<li class="active"><a href="#">Home</a></li>-->
				<li><a href="displayProducts.php">Display Products</a></li>
				<li><a href="editProducts.php">Edit Products</a></li>
				<li><a href="addProducts.php">Add Products</a></li>
				<li><a href="emailForm.php">Contact Us</a></li>
				<li><a href="logout.php">Sign Out</a></li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>
<body>
	<hr>
	<hr>  <div class="container">
      <h1 class="welcome text-center"></h1>
          <div class="card card-container">
            <h2 class="login_title text-center">Confirmation</h2>
						<h3 class="text-center">An email has been sent to <?php echo $email; ?></h3><br>
</body>
</html>
