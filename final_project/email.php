<?php
	class Email 
	{
		// properties
		var $sender;
		var $sendTo;
		var $subject;
		var $message;
		
		// methods
		function setSender($inSender)
		{
			$this->sender = $inSender;
		}
		
		function getSender()
		{
			return $this->sender;
		}
		
		function setSendTo($inSendTo) 
		{
			$this->sendTo = $inSendTo;
		}
		
		function getSendTo() 
		{
			return $this->sendTo;
		}
		
		function setMessage($inMessage) 
		{
			$inMessage=htmlentities($inMessage);
			$inMessage=wordwrap($inMessage, 70,"\n");
			$this->message = $inMessage;
		}
		
		function getMessage() 
		{
			return $this->message;
		}
		
		function setSubject($inSubject) 
		{
			$this->subject = $inSubject;
		}
			
		function getSubject() {
			return $this->subject;
		}
		
		function sendEmail() {
			$headers="From:$this->sender"."\r\n";
			$headers .="Bcc: cdverploeg@dmacc.edu \r\n";
			return mail($this->sender, $this->subject, $this->message, $headers);
		}
		
	}
?>
