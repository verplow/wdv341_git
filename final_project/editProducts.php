<?php

session_start();
if ($_SESSION['validUser'] == "true")
{

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!-- Bootstrap -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<!--end login links-->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-theme.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name-"viewport" content="width=device-width, initial-scale=1"/>
	<title>Edit Products</title>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <!--<a class="" href="#">Admin Home</a>-->
	    </div>
	    <div id="navbar" class="navbar-collapse collapse">
	      <ul class="nav navbar-nav">
	        <!--<li class="active"><a href="#">Home</a></li>-->
					<li><a href="displayProducts.php">Display Products</a></li>
					<li><a href="editProducts.php">Edit Products</a></li>
					<li><a href="addProducts.php">Add Products</a></li>
          <li><a href="emailForm.php">Contact Us</a></li>
					<li><a href="logout.php">Sign Out</a></li>
	      </ul>
	    </div><!--/.nav-collapse -->
	  </div>
	</nav>
	<!--<h3>View / Edit Events</h3>-->

	<div class="page header">
		<h3>View/Edit Products</h3>
	</div>
	<div class="container-fluid">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th class="col-md-1">Item Code</th>
					<th class="col-md-2">Item Name</th>
					<th class="col-md-1">Vendor Name</th>
					<th class="col-md-1">UPC Code</th>
					<th class="col-md-3">Images</th>
					<th class="col-md-1">Cost</th>
					<th class="col-md-1">Price</th>
					<th class="col-md-1">On Hand Amount</th>
					<th class="col-md-1">UPDATE</th>
					<th class="col-md-1">DELETE</th>
				</tr>
			</thead>
			<tbody>
		<!--</table>
	</div>-->
<?php

// run Select statement
try {
	//include "connectPDO.php";
	include "connect.php";
	$stmt = $conn->prepare("SELECT item_code, item_name, vendor_name, upc, item_image, item_cost, item_price, on_hand FROM products");
	$stmt->execute();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
			$images_field = $row['item_image'];
			$images_show = "images/$images_field";
					echo "<tr>";
						echo "<td class='col-md-1'>" . $row['item_code'] . "</td>";
						echo "<td class='col-md-2'>" . $row['item_name'] . "</td>";
						echo "<td class='col-md-1'>" . $row['vendor_name'] . "</td>";
						echo "<td class='col-md-1'>" . $row['upc'] . "</td>";
						echo "<td class='col-md-3'><img src=" .$images_show . "></td>";
						echo "<td class='col-md-1'>" . $row['item_cost'] . "</td>";
						echo "<td class='col-md-1'>" . $row['item_price'] . "</td>";
						echo "<td class='col-md-1'>" . $row['on_hand'] . "</td>";
						echo "<td class='col-md-1'><a href='updateProduct.php?item_id=" . $row['item_id'] . "'>Update</a></td>";
						echo "<td class='col-md-1'><a href='deleteProduct.php?item_id=" . $row['item_id'] . "'>Delete</a></td>";
					echo "</tr>";
	} ?>
		</tbody>
	</table>
</div>
<?php
}
// catch and display Select statement errors
catch (PDOException $e)
{
	echo "An error occurred" .$e->getMessage();
}
}//end valid user
else {
	//invalid user
	header("Location: login.php");
}
?>
