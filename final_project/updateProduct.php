<?php
session_start();

if ($_SESSION['validUser'] == "true") {

$item_code = "";
$item_name = "";
$vendor_name = "";
$upc = "";
$item_image = "";
$item_cost = "";
$item_price = "";
$on_hand = "";

$updateRecId = $_GET['item_id'];	//Record Id to be updated

$validForm = true;  // change back to false

if(isset($_POST["submitForm"]))
{
  //The form has been submitted and needs to be processed

  //Get the name value pairs from the $_POST variable into PHP variables
  //This example uses PHP variables with the same name as the name atribute from the HTML form
  $item_code = $_POST["item_code"];
  $item_name = $_POST["item_name"];
  $vendor_name = $_POST["vendor_name"];
  $upc = $_POST["upc"];
  $item_image = $_POST["item_image"];
  $item_cost = $_POST["item_cost"];
  $item_price = $_POST["item_price"];
  $on_hand = $_POST["on_hand"];

  //VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.
    function validateString($inName)
    {
      global $validForm, $nameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
      $stringErrMsg = "";

      if($inName == "")
      {
        $validForm = false;
        $stringErrMsg = "Field cannot be blank";
      }
    }//end validateString()

    function validateInteger($inInt)
    {
      global $validForm, $intErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
      $intErrMsg = "";
      if(preg_match ("/[^0-9]/", $inInt) || $inInt != "")
      {}
      else
      {
        $validForm = false;
        $intErrMsg = "Invalid entry";
      }
    }//end validateInteger()

    function validateCurrency($inCurrency)
    {
      global $validForm, $currencyErrMsg;
      $currencyErrMsg = "";

      if(is_numeric($inCurrency) || $inCurrency != "")
      {}
      else
      {
        $validForm = false;
        $currencyErrMsg = "Invalid entry";
      }
    }// end validateCurrency()

    //VALIDATE FORM DATA  using functions defined above
    $validForm = true;		//switch for keeping track of any form validation errors

    validateString($item_name);
    validateString($vendor_name);
    validateInteger($upc);
    validateInteger($item_code);
    validateInteger($on_hand);
    validateCurrency($item_cost);
    validateCurrency($item_price);

if($validForm)
{
    $message = "Valid form";
  //}
  try {
    //require 'connectPDO.php';
    include 'connect.php';

    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
    $sql = "UPDATE products SET ";
    $sql .= "item_code='$item_code', ";
    $sql .= "item_name='$item_name', ";
    $sql .= "vendor_name='$vendor_name', ";
    $sql .= "upc='$upc', ";
    $sql .="item_image='$item_image', ";
    $sql .= "item_cost='$item_cost', ";
    $sql .= "item_price='$item_price', ";
    $sql .= "on_hand='$on_hand' ";
    $sql .= "WHERE item_id='$updateRecId'";

    //Display the SQL command to see if it correctly formatted.*/
    //echo "<p>$sql</p>";

    $stmt = $conn->prepare($sql);	//Prepares the query statement
    //Binds the parameters to the query.
    //The ssssis are the data types of the variables in order.
    $stmt->bindParam(':item_code', $item_code);
    $stmt->bindParam(':item_name', $item_name);
    $stmt->bindParam(':vendor_name', $vendor_name);
    $stmt->bindParam(':upc', $upc);
    $stmt->bindParam(':item_image', $item_image);
    $stmt->bindParam(':item_cost', $item_cost);
    $stmt->bindParam(':item_price', $item_price);
    $stmt->bindParam(':on_hand', $on_hand);
    $stmt->execute();

    $result = $stmt;  // hold $query value for if-check so query doesn't execute twice
    //echo "Execute statement";
    //Run the SQL prepared statements
    if ( $result )
    {
    	$message = "<h1>Your record has been successfully updated.</h1>";
    }
    else
    {
      error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
      error_log(var_dump(debug_backtrace()));

      //Clean up any variables or connections that have been left hanging by this error.

      header('Location: files/505_error_response_page.php');	//sends control to a User friendly page
    }

  } // end try
  catch(PDOException $e)
  {
    echo "Connection failed: " . $e->getMessage();
  }

  $conn->close;
  //$connection->close();	//closes the connection to the database once this page is complete.
  }
  else
  {
    $message = "Submission error";
  } // end ifValid check

}// ends ifIsSet
else
{
  //Form has not been seen by the user.  display the form
  try {

    //require 'connectPDO.php';
    require "connect.php";

    // display the record to be edited
    $sql = "SELECT ";
    $sql .= "item_id, item_code, item_name, vendor_name, upc, item_image, item_cost, item_price, on_hand ";
    $sql .= "FROM products ";
    $sql .= "WHERE item_id=$updateRecId";

  	$stmt = $conn->prepare($sql);
  	$stmt->execute();

    //RESULT object contains an associative array
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    $row=$stmt->fetch(PDO::FETCH_ASSOC);

    $item_code=$row['item_id'];
    $item_code=$row['item_code'];
    $item_name=$row['item_name'];
    $vendor_name=$row['vendor_name'];
    $upc=$row['upc'];
    $item_image=$row['item_image'];
    $item_cost=$row['item_cost'];
    $item_price=$row['item_price'];
    $on_hand=$row['on_hand'];
  }
  // catch and display Select statement errors
  catch (PDOException $e)
  {
  	echo "An error occurred" .$e->getMessage();
  }

}
}//end Valid User True
else
{
//Invalid User attempting to access this page. Send person to Login Page
	header('Location: login.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name-"viewport" content="width=device-width, initial-scale=1"/>
<title>Events Form</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!--end login links-->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-theme.min.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

</head>

<body>
<h3>Update Product Form</h3>
  <?php
  if(isset($_POST["submitForm"]))
  {
	//Display the following line when the form has been submitted and
	//the SQL query has successfully updated the database.
  ?>
	 <h1><?php echo $message; ?></h1></br>

   <?php
 }
 else
 {
   ?>

	<h3><?php echo $message; ?></h3>
    <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']) . "?item_id=$updateRecId"; ?>">
      <p>Item Code:
        <label>
          <input type="text" name="item_code" id="item_code" value = "<?php echo $item_code; ?>"/>
          <span><?php echo $stringErrMsg; ?></span>
        </label>
      </p>
      <p>Item Name:
        <label>
          <input type="text" name="item_name" id="item_name" value = "<?php echo $item_name; ?>"/>
          <span><?php echo $stringErrMsg; ?></span>
        </label>
      </p>
      <p>Vendor Name:
        <label>
          <input type="text" name="vendor_name" id="vendor_name" value = "<?php echo $vendor_name; ?>"/>
          <span><?php echo $stringErrMsg; ?></span>
        </label>
      </p>
      <p>UPC code:
        <label>
          <input type="text" name="upc" id="upc" value = "<?php echo $upc; ?>"/>
          <span><?php echo $intErrMsg; ?></span>
        </label>
      </p>
      <p>Image Location:
        <label>
          <input type="text" name="item_image" id="item_image" value = "<?php echo $item_image; ?>"/>
          <span><?php //echo $presenterErrMsg; ?></span>
        </label>
      </p>
      <p>Cost:
        <label>
          <input type="text" name="item_cost" id="item_cost" value="<?php echo $item_cost; ?>"/>
          <span><?php echo $currencyErrMsg; ?></span>
        </label>
      </p>
      <p>Price:
        <label>
          <input type="text" name="item_price" id="item_price" value="<?php echo $item_price; ?>"/>
          <span><?php echo $currencyErrMsg; ?></span>
        </label>
      </p>
      <p>On Hand:
        <label>
          <input type="text" name="on_hand" id="on_hand" value="<?php echo $on_hand; ?>"/>
          <span><?php echo $intErrMsg; ?></span>
        </label>
      </p>
      <p>
        <input type="submit" name="submitForm" id="submitForm" value="Submit" />
        <input type="reset" name="button2" id="button2" value="Reset" />
      </p>
    </form>
    <p>&nbsp;</p>
  <?php
  }
  ?>
</body>
</html>
