-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: May 01, 2018 at 03:20 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `newwdv341`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `item_id` int(10) NOT NULL,
  `item_code` int(10) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `vendor_name` varchar(100) NOT NULL,
  `upc` bigint(12) NOT NULL,
  `item_image` varchar(200) NOT NULL,
  `item_cost` decimal(4,2) NOT NULL,
  `item_price` decimal(5,2) NOT NULL,
  `on_hand` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`item_id`, `item_code`, `item_name`, `vendor_name`, `upc`, `item_image`, `item_cost`, `item_price`, `on_hand`) VALUES
(2, 10134548, 'Assorted Magnets 8 Piece (7364-12)', 'Toysmith', 85761073647, 'magnets.jpg', '1.80', '3.99', 18),
(3, 10151701, 'Cata Pencil (4308-16)', 'Toysmith', 85761202641, 'catapencil.jpg', '1.40', '2.99', 10),
(4, 10191389, 'Catapult Making Kit (3015-6)', 'Toysmith', 85761233300, 'catapult.jpg', '6.00', '11.99', 8),
(5, 10206469, 'Giant Rocket Balloon Set (2534-6)', 'Toysmith', 85761074293, 'rocketballoon.jpg', '5.00', '9.99', 24),
(6, 10206475, 'Paint a Garden Stone (4281-6)', 'Toysmith', 85761219731, 'gardenstone.jpg', '2.50', '6.99', 12),
(7, 10075042, '10\" Baby Penguin 4  assort (02035-12)', 'Wishpets', 637351020359, 'penguins.jpg', '2.81', '7.99', 8),
(9, 10153061, '13\" Mammoth (55700-8)', 'Wishpets', 637351557008, 'mammoth.jpg', '5.50', '12.99', 18),
(10, 10153062, '11\" Grey Owl (95008-6)', 'Wishpets', 637351950083, 'owl.jpg', '6.19', '12.99', 8),
(11, 10196849, '6\" Poseable Kitty Cat Grey (72174-12)', 'Wishpets', 637351721744, 'poseablecat.jpg', '1.99', '4.99', 15),
(12, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(13, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(14, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(15, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(16, 12345, 'test', 'wishpets', 1234, 'test.jpg', '5.00', '9.99', 15),
(17, 12345, 'test', 'wishpets', 1234, 'test.jpg', '5.00', '9.99', 15),
(18, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(19, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(20, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(21, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(22, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(23, 44444, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(24, 44444, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(25, 55555, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(26, 12345, 'test', 'test', 1234, 'test.jpg', '5.00', '9.99', 15),
(27, 2, 'test', 't', 5, '', '0.00', '5.00', 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`item_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
