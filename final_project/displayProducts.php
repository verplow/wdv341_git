<?php
session_start();

if ($_SESSION['validUser'] == "true")
{
  $item_code = "";
  $item_name = "";
  $vendor_name = "";
  $upc = "";
  $item_image = "";
  $item_cost = "";
  $item_price = "";
  $on_hand = "";

    if(isset($_POST["submit"]))
    {
      $vendor_name = $_POST['vendorName'];

      try
      {
        //include 'connectPDO.php';
       include 'connect.php';

        $stmt = $conn->prepare("SELECT item_code, item_name, upc, item_image, item_cost, item_price, on_hand FROM products WHERE vendor_name = :vendor_name");
        $stmt->bindParam(':vendor_name', $vendor_name);
        $stmt->execute();
        $count = $stmt->rowCount();
        ?>
        <body>
          <hr>
          <hr>
          <div class="container">
             <h1 class="welcome text-center"></h1>
            <div class="card card-container">
                   <h2 class="login_title text-center">Product List</h2>
                   <hr>
             <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>">
               <h3 class="text-center">Vendor: <?php echo $vendor_name; ?><br</h3>
               <h5 class="text-center"><?php echo $count; ?> items are available from the selected vendor. </h5><hr>
               <select id="vendorName" name="vendorName" onchange="this.form.submit();">
                 <option value="default" <?php //if ($_POST['program']=='default') echo 'selected="selected";'?>)>---Select Vendor--</option>
                 <option value = "Toysmith">Toysmith</option>
                 <option value = "Wishpets">Wishpets</option>
                 <option value = "test">Test</option>
               </select>
               <hr>
           <div>
               <input type = "submit" name = "submit" value = "Get Vendor Products" />
           </div>
         </div>
       </div> <!--close container-->
        <?php
         if($count > 0)
         {
         //echo '<select name="vendor_list"  id="vendor_list" class="form-control" >';?>
          <div class="container-fluid">
       		   <table class="table table-bordered table-hover">
       			     <thead>
         				<tr>
         					<th class="col-md-1">Item Code</th>
         					<th class="col-md-2">Item Name</th>
         					<th class="col-md-1">UPC Code</th>
         					<th class="col-md-3">Images</th>
         					<th class="col-md-1">Cost</th>
         					<th class="col-md-1">Price</th>
         					<th class="col-md-1">On Hand Amount</th>
         				</tr>
         			</thead>
       			<tbody>
        <?php
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
         {
           $images_field = $row['item_image'];
     			 $images_show = "images/$images_field";
     					echo "<tr>";
     						echo "<td class='col-md-1'>" . $row['item_code'] . "</td>";
     						echo "<td class='col-md-2'>" . $row['item_name'] . "</td>";
     						echo "<td class='col-md-1'>" . $row['upc'] . "</td>";
     						echo "<td class='col-md-3'><img src=" .$images_show . "></td>";
     						echo "<td class='col-md-1'>" . $row['item_cost'] . "</td>";
     						echo "<td class='col-md-1'>" . $row['item_price'] . "</td>";
     						echo "<td class='col-md-1'>" . $row['on_hand'] . "</td>";
              echo "<tr>";
          }//end while block
          ?>
          </tbody>
        </table>
      </div> <?
      }//end if block
      //No rows matched -- do something else
      else
      {
        echo "No rows matched the query.";
      }
  } // end try block
  // catch and display Select statement errors
  catch (PDOException $e)
  {
    echo "An error occurred" . $e->getMessage();
  }// end catch block
}//end isset block
    else
    { ?>
      <hr>
      <div class="container">
         <h1 class="welcome text-center"></h1>
        <div class="card card-container">
               <h2 class="login_title text-center">Product List</h2>
               <hr>
         <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']) ?>">
           <h3 class="text-center">Vendor: <?php echo $vendor_name; ?><br</h3>
           <h5 class="text-center"><?php echo $count; ?> items are available from the selected vendor. <hr>
           <select id="vendorName" name="vendorName" onchange="this.form.submit();">
             <option value="default" <?php //if ($_POST['program']=='default') echo 'selected="selected";'?>)>---Select Vendor--</option>
             <option value = "Toysmith">Toysmith</option>
             <option value = "Wishpets">Wishpets</option>
             <option value = "test">Test</option>
           </select>
           <hr>
       <div>
           <input type = "submit" name = "submit" value = "Get Vendor Products" /></h5>
       </div>
     </div>
   </div> <!--close container-->
  </body>
  <?php
  } // end else
}//end valid user block

else {
  //invalid user
  header("Location:login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!--end login links-->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name-"viewport" content="width=device-width, initial-scale=1"/>
  <title>Today's Events</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!--<a class="navbar-brand" href="#">Admin Home</a>-->
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <!--<li class="active"><a href="#">Home</a></li>-->
          <li><a href="displayProducts.php">Display Products</a></li>
          <li><a href="editProducts.php">Edit Products</a></li>
          <li><a href="addProducts.php">Add Products</a></li>
          <li><a href="emailForm.php">Contact Us</a></li>
          <li><a href="logout.php">Sign Out</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
</body>
</html>
