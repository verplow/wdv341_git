<?php
session_start();

$product_user_name = "";
$product_user_password = "";
$errorMsg = "";
$message = "";
$_SESSION['validUser'] = "";
//if valid user already logged in, display admin options
if($_SESSION['validUser'] == "true")
{
    // admin functions here
  $message = "Welcome " .$product_user_name;
}
else
{
  // if form has been submitted, validate input
  if(isset($_POST["submit"]))
  {
    $product_user_name = $_POST["username"];
    $product_user_password = $_POST["password"];

    // not working
    function testInput($inData)
    {
      $inData = trim($inData);
      $inData = stripslashes($inData);
      $inData = htmlspecialchars($inData);
      return $inData;
    }

    //echo $event_user_name;
    //validate input if not blank
    //if(strlen($event_user_name) > 0 && strlen($event_user_password) > 0)
    //{
      //SQL query to check login against $database
      try
      {
        //include 'connectPDO.php';
        include 'connect.php';

        $sql= "SELECT product_user_name, product_user_password FROM product_user WHERE product_user_name = :product_user_name AND product_user_password = :product_user_password";
        $query = $conn->prepare($sql);
        $query->bindParam(":product_user_name", $product_user_name);
        $query->bindParam(":product_user_password", $product_user_password);
        $query->execute();

        $query->setFetchMode(PDO::FETCH_ASSOC);

  		  $row=$query->fetch(PDO::FETCH_ASSOC);

        //get
  			$validName=$row['product_user_name'];
  			$validPass=$row['product_user_password'];
        //get result count
        //$stmt = "SELECT count() FROM event_user "

        // if entered name and password match table username and password
        if ($product_user_name == $validName && $product_user_password == $validPass)
        {
          $_SESSION['validUser'] = "true";
          ?> <hr> <?php
          $message = "Welcome back, " .$product_user_name;
          //display admin options
        }
        else
        {
          $_SESSION['validUser'] = "false";
          $message = "Invalid login - please re-enter";
        }
      } // end try
      catch (PDOException $e)
      {
        echo $e->getMessage();
      }
    //}
    //else empty, set error message
    /*else
    {
      $errorMsg = "Both fields are required.  Please resubmit";
    }//end validation */
  }
  else
  {
  // has not seen form, display form
  }

  ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <!--login links-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!--end login links-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name-"viewport" content="width=device-width, initial-scale=1"/>
    <title>Administration Login</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"
  </head>
  <body>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="https://use.typekit.net/ayg4pcz.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <div class="container">
      <h1 class="welcome text-center"></h1>
          <div class="card card-container">
            <h2 class="login_title text-center">Administrator Login</h2>
      <?php
      if($_SESSION['validUser'] == "true")
      {
        //echo $errorMsg;
        ?>
        <hr>
        <h3 class="login_title center"><?php echo $message; ?></h3>
        <!--navbar-->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="displayProducts.php">Display Products</a></li>
  					<li><a href="editProducts.php">Edit Products</a></li>
  					<li><a href="addProducts.php">Add Products</a></li>
            <li><a href="emailForm.php">Contact Us</a></li>
  					<li><a href="logout.php">Sign Out</a></li>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
      </nav>
        <?php
      }
      else
      { ?>
          <span id="reauth-email" class="reauth-email alert-danger"><?php echo $message; ?></span>
          <form class="form-signin" id="login" name="login" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
         <!--<div>-->
           <label class="input_title" for="username"><b>User Name: </b></label>
           <input type="text" placeholder="Enter Username" class="login_box" name="username" value="">

           <label class="input_title" for"psw"><b>Password</b></label>
           <input type="password" placeholder="Enter Password" class="login_box" name="password" value="">
           <span><?php echo $errorMsg; ?></span>

           <button class ="btn btn-lg btn-signin" type="submit" name="submit" id="submit" value="Login">Login</button>
           <!--<label>
             <input type="checkbox" checked="checked" name="remember"> Remember me
           </label>-->
         <!--</div>-->

         <!--<div style="background-color: $flflfl">-->
           <button class="btn btn-lg btn-signin" type="button">Cancel</button>
           <!--<span id="reauth-email" class="reauth-email">Forgot <a href="#">password?</a></span>-->
         <!--</div>-->
       </form>
     </div><!--/card container-->
   </div><!--/container-->
     <?php
    }
  } // end else statement
  ?>
  </body>
</html>
