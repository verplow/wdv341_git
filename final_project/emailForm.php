<?php
 	session_start();
  if ($_SESSION['validUser'] == "true")
  { ?>
    <body>
    <hr>
    <div class="container">
      <h1 class="welcome text-center"></h1>
          <div class="card card-container">
            <h2 class="login_title text-center">Contact Us</h2>
          <hr>
        <h5>
          <form action="formHandler.php" method="post">
          </br>
             <div class="row">
              <div class="col-sm-4">
    	           Your name:
              </div>
              <div class="col-sm-5">
    	           <input type="text" name="contactName">
              </div>
            </div>
          </br>
          </br>
             <div class="row">
              <div class="col-sm-4">
                 Your email:
              </div>
              <div class="col-sm-5">
                 <input type="text" name="contactEmail">
              </div>
            </div>
          </br>
          </br>
           <div class="row">
            <div class="col-sm-4">
               Comments:
            </div>
              <div class="col-sm-5">
                <input type="textarea" rows="4" name="comments">
              </div>
          </div>
        </br></h5>
        <p class = "text-center">
          <input type="submit" name="submitForm" id="submitForm" value="Submit" />
          <input type="reset" name="button2" id="button2" value="Reset" />
        </p>
      </form>
        </div>
      </div>
    </body>
        <?php
  }

  else
  {
    //Invalid User attempting to access this page. Send person to Login Page
    header('Location: login.php');
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <!-- Bootstrap -->
  <!--login links-->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <!--end login links-->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name-"viewport" content="width=device-width, initial-scale=1"/>
  <title>Contact Us</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <!--<script src="https://use.typekit.net/ayg4pcz.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>-->
      <!--navbar-->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="displayProducts.php">Display Products</a></li>
          <li><a href="editProducts.php">Edit Products</a></li>
          <li><a href="addProducts.php">Add Products</a></li>
          <li><a href="emailForm.php">Contact Us</a></li>
          <li><a href="logout.php">Sign Out</a></li>

        </ul>
      </div><!--/.nav-collapse -->
  </nav>
</head>
</html>
