-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 29, 2018 at 04:48 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `newwdv341`
--

-- --------------------------------------------------------

--
-- Table structure for table `product_user`
--

CREATE TABLE `product_user` (
  `product_user_id` int(5) NOT NULL,
  `product_user_name` varchar(30) NOT NULL,
  `product_user_password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_user`
--

INSERT INTO `product_user` (`product_user_id`, `product_user_name`, `product_user_password`) VALUES
(1, 'wdv341', 'wdv341');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product_user`
--
ALTER TABLE `product_user`
  ADD PRIMARY KEY (`product_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product_user`
--
ALTER TABLE `product_user`
  MODIFY `product_user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
