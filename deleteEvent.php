<?php
session_start();
if ($_SESSION['validUser'] == "true")
{
  //redirect to selectEvents.php
  header('location:selectEvents.php');
  //include 'connectPDO.php';
  include "connect.php";
  // get 'eventID to delete'
  $id = $_GET['eventID'];
  $stmt = $conn->prepare("DELETE FROM wdv341_event WHERE event_id = $id");
  $stmt -> execute();
  exit();
}
else {
  // invalid username, send to login page
  header("Location: login.php");
}
?>


?>
<h1>Administration Page</h1>
<h2><?php echo "Welcome " .$username ?></h2>
<nav>
  <ul>
      <li><a href="selectEvents.php">Event List</a></li>
      <li><a href="eventsForm2.php">Add Event</a></li>
      <li><a href="logout.php">Sign Out</a></li>
  </ul>
</nav>
<?php
