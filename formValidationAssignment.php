
<?php
	//fields
	$name = "";
	$ssn = "";
	$response = "";
	
	//check for form submission
	if(isset($_POST["submit"]))
	{
		// check honeypot "zip" field
		if($_POST['zip'] != '')
		{
			die("Form Error");
		}
		// get name-value pairs
		$name = $_POST["inName"];
		$ssn = $_POST["inSSN"];
		$response = $_POST["responseType"];
		
		// validation functions
		function validateName($inName)
		{
			global $validForm, $nameErrMsg;
			$nameErrMsg = "";
			
			// checks for name with space or at least one character/number entered
			if(preg_match("/(\w\s)/", $inName)) 
			{
				if (strlen($inName != 0))
				{}
			}		
			else
			{
				$validForm = false;
				$nameErrMsg = "Invalid entry, please enter your name  ";
			}
		}
		
		function validateSSN($inSSN)
		{
			global $validForm, $ssnErrMsg;
			$ssnErrMsg = "";
			
			//remove dashes
			$inSSN = preg_replace("/[^0-9]/","",$inSSN);
			
			//validate SSN is 9 digits, does not allow '666' or '900-999', or '000,' '00,' '0000'; does not allow two known problem SSNs 
			if (!preg_match("/^(?!219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$/", $inSSN))
			{
				$validForm = false;
				$ssnErrMsg = "Invalid entry, please enter your Social Security number  ";	
			}
		}
		
		function validateResponse($inReponse)
		{
			global $validForm, $responseErrMsg;
			$responseErrMsg = "";
			
			if(empty($_POST["responseType"]))
			{
				$validVorm = false;
				$responseErrMsg = "Please select a response option  ";
			}
		}
		
		// is form valid?
		$validForm = true;
		
		validateName($name);
		validateSSN($ssn);
		validateResponse($response);

		echo "Registration complete";
	}
	else
	{
		// form has not been seen, display form
	}
	
	// clear fields
	function clearForm()
	{
		$name = "";
		$ssn = "";
		$response = "";
	}
?>	
<!-- javascript validation -->
<script>
	
	function validName() 
	{
		var name = document.forms["form1"]["inName"].value;
		// allows space in name, "+" checks that at least 1 number or letter is input
		if(/^[\w\s]+$/.test(name) == false && name.length == 0)
		{
			document.getElementById("nameErr").innerHTML="Invalid entry, please enter your name";
		}
	}
	
	function validSoc()
	{
		var ssn = document.forms["form1"]["inSSN"].value;
		if(/^(?!219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}+$/.test(ssn) == false)
		{
			document.getElementById("ssnErr").innerHTML="Invalid entry, please enter your Social Security Number";
		}
	}		
</script>
	
<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;	
}

.test {
	display: none;
}

</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment</h2>
<div id="orderArea">
  <form id="form1" name="form1" method="post" action="formValidationAssignment.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" onblur="validName()" value="" />
      	<span class="error" id="nameErr"></span></td>
      <span class="error"><?php echo $nameErrMsg; ?></span>
    </tr>
    <tr>
      <td>Social Security Number:</td>
      <td><input type="text" name="inSSN" id="inSSN" size="40" onblur="validSoc()" value="" />
      	<span class="error" id="ssnErr"></span></td>
      <span class="error"><?php echo $ssnErrMsg; ?></span>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="responseType" value="phone" required = "required">
          Phone</label>
        <br>
        <label>
          <input type="radio" name="responseType" value="email" required = "required">
          Email</label>
        <br>
        <label>
          <input type="radio" name="responseType" value="USMail" required = "required">
          US Mail</label>
        <br>
      </p>
      <span class="error"><?php echo $responseErrMsg; ?></span></td>
    <p><input class="test" type="text" name="zip" id="zip"/></p>
  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="reset" id="button2" value="ClearForm" onclick="clearForm()"/>
  </p>
</form>
</div>

</body>
</html>