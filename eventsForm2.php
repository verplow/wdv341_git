
<?php
session_start();
if ($_SESSION['validUser'] == "true")
{
  //include "connectPDO.php";
  include "connect.php";

/*$event_name = "";
$event_description = "";
$event_presenter = "";
$event_date = "";
$event_time = "";*/

if(isset($_POST["submitForm"]))
{
  //The form has been submitted and needs to be processed

  //Validate the form data here!

  //Get the name value pairs from the $_POST variable into PHP variables
  //This example uses PHP variables with the same name as the name atribute from the HTML form
  $event_name = $_POST["event_name"];
  $event_description = $_POST["event_description"];
  $event_presenter = $_POST["event_presenter"];
  $event_date = $_POST["event_date"];
  $event_time = $_POST["event_time"];

  //$strToTime = strtotime($event_time);
  //$event_time = date('H:m:s', $strToTime);
  //VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.
    function validateName($inName)
    {
      global $validForm, $nameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
      $nameErrMsg = "";

      if($inName == "")
      {
        $validForm = false;
        $nameErrMsg = "Event name cannot be blank";
      }
    }//end validateName()

    function validateDescription($inDesc)
    {
      global $validForm, $descriptionErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
      $descriptionErrMsg = "";

      if($inDesc == "")
      {
        $validForm = false;
        $descriptionErrMsg = "Description cannot be blank";
      }
    }//end validateDescription()

    function validatePresenter($inPresenter)
    {
      global $validForm, $presenterErrMsg;
      $presenterErrMsg = "";

      if($inPresenter == "")
      {
        $validForm = false;
        $presenterErrMsg = "Presenter cannot be blank";
      }
    }// end validatePresenter()

    function validateDate($inDate)
    {
      global $validForm, $dateErrMsg;
      $dateErrMsg = "";

      if($inDate == null)
      {
        $validForm = false;
        $dateErrMsg = "Please select a date";
      }
    }//end validateDate()

    function validateTime($inTime)
    {
      global $validForm, $timeErrMsg;
      $timeErrMsg = "";

      if($inTime == null)
      {
        $validForm = false;
        $timeErrMsg = "Please select a time";
      }
    }//end validateTime

  //VALIDATE FORM DATA  using functions defined above
  $validForm = true;		//switch for keeping track of any form validation errors

  validateName($event_name);
  validateDescription($event_description);
  validatePresenter($event_presenter);
  validateDate($event_date);
  validateTime($event_time);

  /*if($validForm)
  {
    $message = "All good";
  }
  else
  {
    $message = "Something went wrong";
  }*/


/*
This form is self-posting in order to process the validations.   It will use the following algorithm or process
in order to properly display and validate the form and its data.

if the form has been submitted		(The user has filled out the form and hit the submit button)
  {
  then validate the form data		(The form data is ready to be validated)

  //Validation Algorithm				(The validation process will follow the following process or set of steps)
  set validForm = true					Set a flag or switch to true.  This assumes the form data is valid.
    perform validateName()				This will validate the data from the Name field
    perform validateQuantity()			This will validate the data from the Quantity field
    perform validateEmail()				This will validate the data from the Email field
    perform validateShipping()			This will validate the data from the Shipping field
  if (validForm==true)				If the flag is still true no errors were found, the form is valid
    {
    move form data into database		The form data is good so it should be INSERTED into the database
    }
  else								The flag is false because errors were found in the data
    {
    load data back into the form		Put the data back into the form fiels so the user can see what was in the fields
    load the error messages				Place the appropriate error messages on the form so the user knows what to fix
    display the form					Display the form with its original data and error messages to the user.
    }
  }
else
  {
  display the form				(The user needs to enter data on the form so it can be validated and processed)
  }
*/

/*
field validation algorithm		This process is done for each field validation function.  The details change for each field but the
                same steps are processed in the same order for each validation.

  clear the error messages for this validation.  Set to ""		(Cleans up from previous errors and assumes there will not be an error)
  check the variable for the field against the expected values
  if it meets those values
    {
    the field is valid
    nothing else needs done
    }
  else
    {
    the field is invalid
    set the validForm=false			(A data validation error has been found.  The form is no longer valid)
    set the error message variable for this field to the appropriate message
    }
*/
  //include 'dbConnect.php';	//connects to the database
  //Create the SQL command string
  /*$sql = "INSERT INTO wdv341 (";
  $sql .= "event_name, ";
  $sql .= "event_description, ";
  $sql .="event_presenter, ";
  $sql .= "event_date, ";
  $sql .= "event_time";	//Last column does NOT have a comma after it.
  $sql .= ") VALUES (?,?,?,?,?)";	//? Are placeholders for variables*/

  //Display the SQL command to see if it correctly formatted.
  //echo "<p>$sql</p>";

  //$query = $connection->prepare($sql);	//Prepares the query statement

  //Binds the parameters to the query.
  //The ssssis are the data types of the variables in order.
  //$query->bind_param("ssssis",$presenter_first_name,$presenter_last_name,$presenter_city,$presenter_st,$presenter_zip,$presenter_email);

  //Run the SQL prepared statements
  //if ( $query->execute() )
  //{
  //	$message = "<h1>Your record has been successfully added to the database.</h1>";
  //	$message .= "<p>Please <a href='presentersSelectView.php'>view</a> your records.</p>";
  //}
  //else
  //{
  //	$message = "<h1>You have encountered a problem.</h1>";
  //	$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";	//remove this for production purposes
  //}
if($validForm)
{
    $message = "All good";
  //}
  try {
    //include "connectPDO.php";
    include "connect.php";
    //echo "Connected successfully";
    $event_time = date('H:i', strToTime($event_time));

    $sql = "INSERT INTO wdv341_event (";
    $sql .= "event_name, ";
    $sql .= "event_description, ";
    $sql .="event_presenter, ";
    $sql .= "event_date, ";
    $sql .= "event_time";	//Last column does NOT have a comma after it.
    $sql .= ") VALUES (:event_name, :event_description, :event_presenter, :event_date, :event_time)";

    //Display the SQL command to see if it correctly formatted.*/
    echo "<p>$sql</p>";

    $query = $conn->prepare($sql);	//Prepares the query statement
    //Binds the parameters to the query.
    //The ssssis are the data types of the variables in order.
    $query->bindParam(':event_name', $event_name);
    $query->bindParam(':event_description', $event_description);
    $query->bindParam(':event_description', $event_description);
    $query->bindParam(':event_presenter', $event_presenter);
    $query->bindParam(':event_date', $event_date);
    $query->bindParam(':event_time', $event_time);
    $query->execute();

    $result = $query;  // hold $query value for if-check so query doesn't execute twice
    echo "Execute statement";
    //Run the SQL prepared statements
    if ( $result )
    {
    	$message = "<h1>Your record has been successfully added to the database.</h1>";
    	//$message .= "<p>Please <a href='presentersSelectView.php'>view</a> your records.</p>";
    }
    else
    {
    	$message = "<h1>You have encountered a problem.</h1>";
    	//$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";	//remove this for production purposes
    }
  	/*$stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
  	VALUES(:event_name, :event_description, :event_presenter, :event_date, :event_time)");
  	$stmt->bindParam(':event_name', $event_name);
  	$stmt->bindParam(':event_description', $event_description);
    $stmt->bindParam(':event_presenter', $event_presenter);
    $stmt->bindParam(':event_date', $event_date);
    $stmt->bindParam(':event_time', $event_time);
    $stmt->execute();

    if ( $stmt->execute() )
    {
      $message  = "<h1>Your event has been successfully added to the database.</h1>";
      //$message .= "<p>Please <a href='presentersSelectView.php'>view</a> your records.</p>";
    }
    else
    {
    	$message "<h1>You have encountered a problem.</h1>";
    	//$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";	//remove this for production purposes
    }

  	//echo "New record entered"; */
  } // end try
  catch(PDOException $e)
  {
    echo "Connection failed: " . $e->getMessage();
  }

  $conn->close;
  //$connection->close();	//closes the connection to the database once this page is complete.
  }
  else
  {
    $message = "Submission error";
  } // end ifValid check

}// ends ifIsSet
else
{
  //Form has not been seen by the user.  display the form
  $message = "Please fill out form";
}
}//end Valid User True
else
{
//Invalid User attempting to access this page. Send person to Login Page
	header('Location: login.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name-"viewport" content="width=device-width, initial-scale=1"/>
<title>Events Form</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
  $(function() {
    $("#event_date").datepicker({dateFormat: "yy-mm-dd"})
      minDate: 0  // event date has to be current
  });
  $(function() {
    $("#event_time").timepicker({timeFormat: "h:mm p"})
  });
</script>
</head>
<nav>
    <ul>
        <li><a href="login.php">Sign In</a></li>
        <li><a href="selectEvents.php">Edit Events</a></li>
        <li><a href="displayEvents.php">Today's Events</a></li>
        <li><a href="logout.php">Sign Out</a></li>
    </ul>
  <div class="clearFloat"></div>

</nav>
<body>
<h2>WDV341 Intro PHP</h2>
<h3>Event Entry Form</h3>
  <?php
  if(isset($_POST["submitForm"]))
  {
	//Display the following line when the form has been submitted and
	//the SQL query has successfully updated the database.
  ?>
	 <h1><?php echo $message; ?></h1></br>

   <?php
 }
 else
 {
	//Display the following lines if the page is called from a link.
	//The user has not seen the form yet and needs to see the form.
	//This will display the form, allow the user to enter data, then submit the form*/
  ?>
	<h3><?php echo $message; ?></h3>
    <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
      <p>Event name:
        <label>
          <input type="text" name="event_name" id="event_name" value = ""/>
          <span><?php echo $nameErrMsg; ?></span>
        </label>
      </p>
      <p>Event Description:
        <label>
          <input type="text" name="event_description" id="event_description" value = ""/>
          <span><?php echo $descriptionErrMsg; ?></span>
        </label>
      </p>
      <p>Presenter:
        <label>
          <input type="text" name="event_presenter" id="event_presenter" value = ""/>
          <span><?php echo $presenterErrMsg; ?></span>
        </label>
      </p>
      <p>Date:
        <label>
          <input type="text" name="event_date" id="event_date" value=""/>
          <span><?php echo $dateErrMsg; ?></span>
        </label>
      </p>
      <p>Time:
        <label>
          <input type="text" name="event_time" id="event_time" value=""/>
          <span><?php echo $timeErrMsg; ?></span>
        </label>
      </p>
      <p>
        <input type="submit" name="submitForm" id="submitForm" value="Submit" />
        <input type="reset" name="button2" id="button2" value="Reset" />
      </p>
    </form>
    <p>&nbsp;</p>
  <?php
  }
  ?>
</body>
</html>
