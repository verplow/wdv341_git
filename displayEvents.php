<?php
session_start();
if ($_SESSION['validUser'] == "true")
{
  try {
     // get today's date
     //include 'connectPDO.php';
     include 'connect.php';

     date_default_timezone_set('America/Chicago');
     $currDate = date('Y-m-d');
     //echo $currDate;
	   $stmt = $conn->prepare("SELECT event_id, event_name, event_description, event_date, event_time FROM wdv341_event WHERE event_date= :currDate");
     $stmt->bindParam(":currDate", $currDate);
     $stmt->execute();
     // return number of events today
     $count = $stmt->rowCount();

     if($count > 0)
     {
       while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
       {
         $eventID = $row['event_id'];
         $eventName = $row['event_name'];
         $eventDesc = $row['event_description'];
         $eventDay = $row['event_date'];
         $eventTime = $row['event_time'];
         //reformat time for 12 hour am/pm
         $eventTime = date('h:i a', strtotime($eventTime));
       }
     }
     //No rows matched -- do something else
     else {
      echo "No rows matched the query.";
    }
  }
  // catch and display Select statement errors
  catch (PDOException $e)
  {
    echo "An error occurred" . $e->getMessage();
  }
}
else {
  //invalid user
  header("Location:login.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Display Events Example</title>
    <style>
		.eventBlock{
			width:500px;
			margin-left:auto;
			margin-right:auto;
			background-color:#CCC;
		}

		.displayEvent{
			text_align:left;
			font-size:18px;
		}

		.displayDescription {
			margin-left:100px;
		}
	</style>
</head>
<nav>
  <ul>
    <li><a href="login.php">Sign In</a></li>
    <li><a href="selectEvents.php">Edit Events</a></li>
    <li><a href="eventsForm2.php">Add Event</a></li>
    <li><a href="logout.php">Sign Out</a></li>
  </ul>
  <div class="clearFloat"></div>
</nav>
<body>
    <h1>WDV341 Intro PHP</h1>
    <h2>Example Code - Display Events as formatted output blocks</h2>
    <h3> <?php echo $count; ?> Events are available today.</h3>

<?php
	//Display each row as formatted output
//while( $query->fetch() )
	//Turn each row of the result into an associative array
  	//{
		//For each row you have in the array create a block of formatted text*/
?>
	<p>
        <div class="eventBlock">
            <div>
            	<span class="displayEvent">Event: "<?php echo $eventName; ?>"</span>
            	<span class="displayDescription">Description: <?php echo $eventDesc; ?></span>
            </div>
            <div>
            	<span class="displayTime">Time: <?php echo $eventTime; ?>< <</span>
            </div>
            <div>
            	<span class="displayDate">Date: <?php echo $eventDay; ?></span>
            </div>
        </div>
    </p>

<?php
  	//}//close while loop
	$query->close();
	$connection->close();	//Close the database connection
?>
</div>
</body>
</html>
