<?php

include "connect.php";

?>

<table border='1'>
	<tr>
		<td>ID</td>
		<td>Name</td>
		<td>Description</td>
		<td>UPDATE</td>
		<td>DELETE</td>
	</tr>
<?php

// run Select statement
try {

	$stmt = $conn->prepare("SELECT event_id, event_name, event_description FROM wdv341_event");
	$stmt->execute();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		echo "<tr>";
			echo "<td>" . $row['event_id'] . "</td>";
			echo "<td>" . $row['event_name'] . "</td>";
			echo "<td>" . $row['event_description'] . "</td>";
			//echo "<td><a href='updateEvent.php?eventID=" . $row['event_id'] . "'>Update</a></td>";
			echo "<td><a href='deleteEvent.php?eventID=" . $row['event_id'] . "'>Delete</a></td>";
		echo "</tr>";
	}
}
// catch and display Select statement errors
catch (PDOException $e)
{
	echo "An error occurred" .$e->getMessage();
}
?>
</table>
