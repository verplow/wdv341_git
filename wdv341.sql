-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 08, 2018 at 09:06 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `newwdv341`
--
-- --------------------------------------------------------

--
-- Table structure for table `wdv341_event`
--

CREATE TABLE `wdv341_event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `event_description` varchar(400) NOT NULL,
  `event_presenter` varchar(100) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wdv341_event`
--

INSERT INTO `wdv341_event` (`event_id`, `event_name`, `event_description`, `event_presenter`, `event_date`, `event_time`) VALUES
(1, 'Job Fair', 'IT Career Day', '', '0000-00-00', '00:00:00'),
(2, 'Potluck', 'Administration Potluck Party', '', '0000-00-00', '00:00:00'),
(3, 'Mud Run', '30th annual mud run', '', '0000-00-00', '00:00:00'),
(8, 'Fun Run', '5k race', 'Run4Progress', '0000-00-00', '00:00:00'),
(9, 'Fun Run', '5k race', 'Run4Progress', '0000-00-00', '00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wdv341_event`
--
ALTER TABLE `wdv341_event`
  ADD PRIMARY KEY (`event_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wdv341_event`
--
ALTER TABLE `wdv341_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
