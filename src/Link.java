
public class Link {
	//fields
	public StoreData sData;	// object holding stores
	public VendorData vData; // object holding vendors
	public Link next;		// next link in list

	int sNum = sData.getStoreNumber();
	String sName = sData.getStoreName();
	String sDistrict = sData.getDistrict();
	
	String vID = vData.getVendorID();
	String vName = vData.getVendorName();
	String vType = vData.getVendorType();
	
	/**
	 * @param inStoreData  new store object (number, name, district)
	 * @param inVendData   new vendor object (ID, name, type)
	 */
	public Link(StoreData inStoreData, VendorData inVendData)
	{
		sData = inStoreData;
		vData = inVendData;
	}
	
	/**
	 * print store numb
	 */
	public void displayLink()
	{	
		System.out.println("Store number: " + sNum + " Store Name: " + sName + " District: " + sDistrict);
		System.out.println("Vendor ID: " + vID + " Vendor Name: " + vName + " Vendor Type: " + vType);
	}
}
