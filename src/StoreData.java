
public class StoreData {
	//fields
	private int storeNumber;		
	private String storeName; // store name = location
	private String district;	 // breaks stores up into regional districts
	
	/**
	 * @param inStoreNum		the store number to set
	 * @param inStoreName	the store name to set
	 * @param inDistrict		the store district to set
	 */
	public StoreData(int inStoreNum, String inStoreName, String inDistrict)
	{
		this.storeNumber = inStoreNum;
		this.storeName = inStoreName;
		this.district = inDistrict;
	}

	/**
	 * default constructor
	 */
	public StoreData()
	{}
	
	/**
	 * @return the storeNumber
	 */
	public int getStoreNumber() {
		return storeNumber;
	}

	/**
	 * @param storeNumber the storeNumber to set
	 */
	public void setStoreNumber(int storeNumber) {
		this.storeNumber = storeNumber;
	}

	/**
	 * @return the storeName
	 */
	public String getStoreName() {
		return storeName;
	}

	/**
	 * @param storeName the storeName to set
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	/**
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * @param district the district to set
	 */
	public void setDistrict(String district) {
		this.district = district;
	}
	
	/**
	 * @return StoreData 
	 */
	public String toString()
	{
		return "Store number: [" + storeNumber + "], [Store name: " + storeName 
				+ "], [District: " + district + "]";
	}
}
