
public class VendorData {
	//fields
	private String vendorID;
	private String vendorName;
	private String vendorType;
	
	/**
	 * @return the vendorID
	 */
	public String getVendorID() {
		return vendorID;
	}
	/**
	 * @param vendorID the vendorID to set
	 */
	public void setVendorID(String inVendorID) {
		this.vendorID = inVendorID;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String inVendorName) {
		this.vendorName = inVendorName;
	}
	/**
	 * @return the vendorType
	 */
	public String getVendorType() {
		return vendorType;
	}
	/**
	 * @param vendorType the vendorType to set
	 */
	public void setVendorType(String inVendorType) {
		this.vendorType = inVendorType;
	}
	
	public String toString() {
		return "Vendor ID: [" + vendorID + "], Vendor name: [" + vendorName + "], Vendor Type [" + vendorType + "]";
	}
}
