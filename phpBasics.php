<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - PHP Basics</title>
</head>

<body>

<h1>WDV341 Intro PHP</h1>

<!-- GIT REPO ADDRESS:  https://verplow@bitbucket.org/verplow/wdv341_git.git -->

<p>1. Create a variable called yourName.  Assign it a value of your name.</p>
<?php
	$yourName = "Cari Benesh";
?>

<p>2.  Display the assignment name in an h1 element on the page.  Include the elements in your output.
<?php 
	echo "<h1>". $yourName . "</h1>";
?>
</p>

<p>3.  Use HTML to put an h2 element on the page.  Use PHP to display your name inside the element using the variable.
<h2><?php 
	echo $yourName;
?>
</h2></p>

<p>4.  Create the following variables:  number1, number2, and total. Assign a value to them.</p>
<?php 
	$number1 = 10; 
	$number2 = 8;
	$total = $number1 + $number2;
?>

<p>5.  Display the value of each variable and the total variable when you add them together.
<?php 
	echo "<p>Number 1 is " . $number1 . "</p> <p> Number 2 is " . $number2 . "</p> <p> Total is " . $total . ".</p>"
?>
</p>

<p>Use PHP to create a Javascript array with the following values: PHP,HTML,Javascript.  
Output this array using PHP.  Create a script that will display the values of this array on your page.  
NOTE:  Remember PHP is building the array not running it.</p>
<p>
<?php
	$phpArray = array("PHP","HTML","Javascript");

	$jscriptArray = json_encode($phpArray);
	
	echo "PHP output: " .$jscriptArray;
?></p>
<p id="displayArray"></p>
	<script type = "text/javascript">
		var par = document.getElementById("displayArray");	
		var jsArray = <?php echo json_encode($phpArray); ?>;
		par.textContent = "Script output: " + jsArray.toString();	
	</script>

</body>
</html>