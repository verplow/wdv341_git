<?PHP
  $serverName = "localhost";
  $username = "wdvdemo341";
  $password = "1ntroPHPrun8";
  $database = "newwdv341";

  try {
    $conn = new PDO("mysql:host=$serverName;dbname=$database",$username,$password);

    // set PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
  }

  catch(PDOException $e)
  {
    echo "Connection failed: " . $e->getMessage();
    exit;
  }
?>
