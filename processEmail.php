
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP Class Example</title>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>PHP Functions - Email Class</h2>
<p>&nbsp;</p>
<h3>Example Email</h3>
<p>
<?php include("email.php"); ?>

<?php
	//3. Create/Instantiate a new Email object called newEmail.
	$newEmail = new email();
	//4. Use the setter methods to load information into your newEmail object.
	$newEmail->setSender("jane@doe.com");
	$newEmail->setSendTo("charlie@charlieparker.org");
	$newEmail->setMessage("Where's your saxophone?");
	$newEmail->setSubject("Help");
	
	//5.  Use the getter methods to output and display the contents of the newEmail object onto your page.  
	echo "From: " . $newEmail->getSender() . " </br>"; 
	echo "To: " . $newEmail->getSendTo() . "</br>";
	echo "Subject: " . $newEmail->getSubject() . "</br>"; 
	echo "Message: " . $newEmail->getMessage() . "</br> </br>"; 
?>

</p>
</body>
</html>