<?php

session_start();
if ($_SESSION['validUser'] == "true")
{

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name-"viewport" content="width=device-width, initial-scale=1"/>
<title>Events Form</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

</head>
	<nav>
		<ul>
			<li><a href="login.php">Sign In</a></li>
			<li><a href="displayEvents.php">Today's Events</a></li>
			<li><a href="eventsForm2.php">Add Event</a></li>
			<li><a href="logout.php">Sign Out</a></li>
		</ul>
  	<div class="clearFloat"></div>
	</nav>
<body>
<h2>WDV341 Intro PHP</h2>
<h3>View / Edit Events</h3>

<table border='1'>
	<tr>
		<td>ID</td>
		<td>Name</td>
		<td>Description</td>
		<td>UPDATE</td>
		<td>DELETE</td>
	</tr>
<?php

// run Select statement
try {
	//include "connectPDO.php";
	include "connect.php";
	$stmt = $conn->prepare("SELECT event_id, event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event");
	$stmt->execute();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		echo "<tr>";
			echo "<td>" . $row['event_id'] . "</td>";
			echo "<td>" . $row['event_name'] . "</td>";
			echo "<td>" . $row['event_description'] . "</td>";
			echo "<td>" . $row['event_presenter'] . "</td>";
			echo "<td>" . $row['event_date'] . "</td>";
			echo "<td>" . $row['event_time'] . "</td>";
			echo "<td><a href='updateEvent.php?eventID=" . $row['event_id'] . "'>Update</a></td>";
			echo "<td><a href='deleteEvent.php?eventID=" . $row['event_id'] . "'>Delete</a></td>";
		echo "</tr>";
	}
}
// catch and display Select statement errors
catch (PDOException $e)
{
	echo "An error occurred" .$e->getMessage();
}
}//end valid user
else {
	//invalid user
	header("Location: login.php");
}
?>
</table>
