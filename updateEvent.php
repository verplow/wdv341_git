<?php
session_start();

if ($_SESSION['validUser'] == "true") {

$event_name = "";
$event_description = "";
$event_presenter = "";
$event_date = "";
$event_time = "";

$updateRecId = $_GET['eventID'];	//Record Id to be updated

$validForm = false;

if(isset($_POST["submitForm"]))
{
  //The form has been submitted and needs to be processed

  //Get the name value pairs from the $_POST variable into PHP variables
  //This example uses PHP variables with the same name as the name atribute from the HTML form
  $event_name = $_POST["event_name"];
  $event_description = $_POST["event_description"];
  $event_presenter = $_POST["event_presenter"];
  $event_date = $_POST["event_date"];
  $event_time = $_POST["event_time"];

  //$strToTime = strtotime($event_time);
  //$event_time = date('H:m:s', $strToTime);
  //VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.
    function validateName($inName)
    {
      global $validForm, $nameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
      $nameErrMsg = "";

      if($inName == "")
      {
        $validForm = false;
        $nameErrMsg = "Event name cannot be blank";
      }
    }//end validateName()

    function validateDescription($inDesc)
    {
      global $validForm, $descriptionErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
      $descriptionErrMsg = "";

      if($inDesc == "")
      {
        $validForm = false;
        $descriptionErrMsg = "Description cannot be blank";
      }
    }//end validateDescription()

    function validatePresenter($inPresenter)
    {
      global $validForm, $presenterErrMsg;
      $presenterErrMsg = "";

      if($inPresenter == "")
      {
        $validForm = false;
        $presenterErrMsg = "Presenter cannot be blank";
      }
    }// end validatePresenter()

    function validateDate($inDate)
    {
      global $validForm, $dateErrMsg;
      $dateErrMsg = "";

      if($inDate == null)
      {
        $validForm = false;
        $dateErrMsg = "Please select a date";
      }
    }//end validateDate()

    function validateTime($inTime)
    {
      global $validForm, $timeErrMsg;
      $timeErrMsg = "";

      if($inTime == null)
      {
        $validForm = false;
        $timeErrMsg = "Please select a time";
      }
    }//end validateTime

  //VALIDATE FORM DATA  using functions defined above
  $validForm = true;		//switch for keeping track of any form validation errors

  validateName($event_name);
  validateDescription($event_description);
  validatePresenter($event_presenter);
  validateDate($event_date);
  validateTime($event_time);

if($validForm)
{
    $message = "All good";
  //}
  try {
    //require 'connectPDO.php';
    include 'connect.php';

    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
    $event_time = date('H:i a', strToTime($event_time));

    $sql = "UPDATE wdv341_event SET ";
    $sql .= "event_name='$event_name', ";
    $sql .= "event_description='$event_description', ";
    $sql .="event_presenter='$event_presenter', ";
    $sql .= "event_date='$event_date', ";
    $sql .= "event_time='$event_time' ";	//Last column does NOT have a comma after it.
    $sql .= "WHERE event_id='$updateRecId'";

    //Display the SQL command to see if it correctly formatted.*/
    echo "<p>$sql</p>";

    $stmt = $conn->prepare($sql);	//Prepares the query statement
    //Binds the parameters to the query.
    //The ssssis are the data types of the variables in order.
    $stmt->bindParam(':event_name', $event_name);
    $stmt->bindParam(':event_description', $event_description);
    $stmt->bindParam('event_description', $event_description);
    $stmt->bindParam(':event_presenter', $event_presenter);
    $stmt->bindParam(':event_date', $event_date);
    $stmt->bindParam(':event_time', $event_time);
    $stmt->execute();

    $result = $stmt;  // hold $query value for if-check so query doesn't execute twice
    //echo "Execute statement";
    //Run the SQL prepared statements
    if ( $result )
    {
    	$message = "<h1>Your record has been successfully updated.</h1>";
    	//$message .= "<p>Please <a href='presentersSelectView.php'>view</a> your records.</p>";
    }
    else
    {
      error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
      error_log(var_dump(debug_backtrace()));

      //Clean up any variables or connections that have been left hanging by this error.

      header('Location: files/505_error_response_page.php');	//sends control to a User friendly page
    }

  } // end try
  catch(PDOException $e)
  {
    echo "Connection failed: " . $e->getMessage();
  }

  $conn->close;
  //$connection->close();	//closes the connection to the database once this page is complete.
  }
  else
  {
    $message = "Submission error";
  } // end ifValid check

}// ends ifIsSet
else
{
  //Form has not been seen by the user.  display the form
  try {

    //require 'connectPDO.php';
    require "connect.php";

    // display the record to be edited
    $sql = "SELECT ";
    $sql .= "event_id, event_name, event_description, event_presenter, event_date, event_time ";
    $sql .= "FROM wdv341_event ";
    $sql .= "WHERE event_id=$updateRecId";

  	$stmt = $conn->prepare($sql);
  	$stmt->execute();

    //RESULT object contains an associative array
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    $row=$stmt->fetch(PDO::FETCH_ASSOC);

    $event_name=$row['event_name'];
    $event_description=$row['event_description'];
    $event_presenter=$row['event_presenter'];
    $event_date=$row['event_date'];
    $event_time=$row['event_time'];

    //reformat time output
    $event_time=date('h:i a', strToTime($event_time));

  }
  // catch and display Select statement errors
  catch (PDOException $e)
  {
  	echo "An error occurred" .$e->getMessage();
  }

}
}//end Valid User True
else
{
//Invalid User attempting to access this page. Send person to Login Page
	header('Location: login.php');
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name-"viewport" content="width=device-width, initial-scale=1"/>
<title>Events Form</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
  $(function() {
    $("#event_date").datepicker({dateFormat: "yy-mm-dd"})
      minDate: 0  // event date has to be current
  });
  $(function() {
    $("#event_time").timepicker({timeFormat: "h:mm p"})
  });
</script>
</head>

<body>
<h2>WDV341 Intro PHP</h2>
<h3>Update Event Form</h3>
  <?php
  if(isset($_POST["submitForm"]))
  {
	//Display the following line when the form has been submitted and
	//the SQL query has successfully updated the database.
  ?>
	 <h1><?php echo $message; ?></h1></br>

   <?php
 }
 else
 {
   ?>

	<h3><?php echo $message; ?></h3>
    <form id="form1" name="form1" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']) . "?eventID=$updateRecId"; ?>">
      <p>Event name:
        <label>
          <input type="text" name="event_name" id="event_name" value = "<?php echo $event_name; ?>"/>
          <span><?php echo $nameErrMsg; ?></span>
        </label>
      </p>
      <p>Event Description:
        <label>
          <input type="text" name="event_description" id="event_description" value = "<?php echo $event_description; ?>"/>
          <span><?php echo $descriptionErrMsg; ?></span>
        </label>
      </p>
      <p>Presenter:
        <label>
          <input type="text" name="event_presenter" id="event_presenter" value = "<?php echo $event_presenter; ?>"/>
          <span><?php echo $presenterErrMsg; ?></span>
        </label>
      </p>
      <p>Date:
        <label>
          <input type="text" name="event_date" id="event_date" value="<?php echo $event_date; ?>"/>
          <span><?php echo $dateErrMsg; ?></span>
        </label>
      </p>
      <p>Time:
        <label>
          <input type="text" name="event_time" id="event_time" value="<?php echo $event_time; ?>"/>
          <span><?php echo $timeErrMsg; ?></span>
        </label>
      </p>
      <p>
        <input type="submit" name="submitForm" id="submitForm" value="Submit" />
        <input type="reset" name="button2" id="button2" value="Reset" />
      </p>
    </form>
    <p>&nbsp;</p>
  <?php
  }
  ?>
</body>
</html>
